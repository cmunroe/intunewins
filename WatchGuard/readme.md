# WatchGuard Mobile SSL VPN 12.10

## Deployment Variables

### App Information
* Name: WatchGuard SSL VPN 12.10
* Description: VPN to access internal work resources.
* Publisher: WatchGuard
* Version: 12.10
* Category: Business

### Program
* Install: WatchGuard_SSLVPN_12_10.exe /SILENT /VERYSILENT /NORESTART
* Uninstall: "C:\Program Files (x86)\WatchGuard\WatchGuard Mobile VPN with SSL\unins000.exe" /SILENT /VERYSILENT

### Requirements
* OS: 64-Bit
* Min OS: Win 10 22H2
* Min Disk: 1024

### Detection Rule
* Method: Manual
* Type: File
* Path: C:\Program Files (x86)\WatchGuard\WatchGuard Mobile VPN with SSL
* File: wgsslvpnc.exe
* Detection Method: String (version)
* Operator: Equals
* Value: 12.10.0.0

### Supersedence
* Method: Update