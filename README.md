# CI/CD Makes IntuneWin Better

First off, Microsoft... Why do we need to make an IntuneWin in the first place? Please make it stop. We are repacking a pack that already has a proper package.... msi.

Without the above being fixed, making an IntuneWin file for each new version can be cumbersome. It's also cumbersome in regards to remembering the steps we needed to take previously if special steps were needed.

Additionally, we just want to make version changes and let our build servers take care of the rest for us! No more tying up our Laptops / Desktops with the tedious task of downloading, running the IntuneWin application, and then uploading the results.

Finally, It's probably more secure if we make this into code with a CI/CD pipeline. Our personal devices can be compromised or attacked with a virus. Possibly with the same application, we are trying to craft into an IntuneWin. CI/CD pipelines are ephemeral meaning a bad build or corruption previously won't affect our future or other builds.

## Goals

* Common applications needed in the business world.
* Monthly releases (I hope).
* Deployment instructions to make it easier to get Intune Apps up and running.
* Sustainable Build Practices.

## Application Versions

| Software | Version |
| -------- | ------- |
| Firefox  | Latest  |
| 7-Zip  | 24.07  |
| KeePass | 2.57 |
| Putty  | 0.81  |
| PyCharm Community  | 2024.01  |
| VSCode | 1.91.1 | 
| WatchGuard Mobile SSL VPN  | 12.10 |


## Caveats

I am currently only going to build native 64-bit applications, with backward support for 32-bit applications if no 64-bit application is available. It is honestly 2024, and if we are using 32-bit clients in a work environment with 4GB of RAM you probably don't need this. 

Some of the build processes will use my internal CI/CD build servers to save runner minutes to be used for actual useful things. Such as the actual build of the IntuneWin files, and not the simple moving around of data.

